from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


@login_required
def list_receipts(request):
    receipt_objects = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipt_objects}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def account_list(request):
    account_objects = Account.objects.filter(owner=request.user)
    context = {"accounts": account_objects}
    return render(request, "receipts/accounts.html", context)


@login_required
def category_list(request):
    expense_objects = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expenses": expense_objects}
    return render(request, "receipts/expenses.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "receipts/create_account.html", context)


def redirect_list(request):
    return redirect("home")
