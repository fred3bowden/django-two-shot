from django.shortcuts import render, redirect
from accounts.forms import AccountForm, SignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


def login_form(request):
    form = AccountForm()
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home")
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "accounts/login.html", context)


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")

    else:
        form = SignupForm()

    context = {"form": form}

    return render(request, "accounts/signup.html", context)


def logout_view(request):
    logout(request)

    return redirect("login")
