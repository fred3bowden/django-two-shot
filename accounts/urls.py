from django.urls import path
from accounts.views import login_form, logout_view, signup_view

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup_view, name="signup"),
]
